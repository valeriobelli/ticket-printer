window.addEventListener("load", () => {
  new Vue({
    el: "#container",
    data: {
      errorMessage: "",
      fetch: false,
      fetchTest: false,
    },
    template: `
      <div id="container">
        <div class="error-message" v-show="errorMessage != ''">
            {{ errorMessage }}
        </div>
        <div class="button" v-on:click="handleOnClick" v-bind:class="{ fetching: fetch }">
            <div class="sk-circle" v-if="fetch == true">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
            </div>
            <h1 class="text" v-else>
                Clicca qui<br />X
            </h1>
        </div>
        <div class="test-button" v-on:click="handleOnClickTest" v-bind:class="{ fetching: fetchTest }">
            <div class="sk-circle" v-if="fetchTest == true">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
            </div>
            <h5 class="text" v-else>
                Test
            </h5>
        </div>
      </div>
    `,
    methods: {
      handleOnClick() {
        if (!this.fetch) {
          this.fetch = true;
          fetch("/api/v1/print")
            .then((response) => {
              if (response.status !== 200) {
                switch (response.status) {
                  case 401:
                    break;
                  case 500:
                  default:
                    this.errorMessage =
                      "È avvenuto un errore durante la comunicazione con la stampante.";
                    setTimeout(() => {
                      this.errorMessage = "";
                    }, 4000);
                }
              }

              this.fetch = false;
            })
            .catch((err) => {
              console.log(err);
              this.fetch = false;
            });
        }
      },
      handleOnClickTest() {
        if (!this.fetchTest) {
          this.fetchTest = true;
          fetch("/api/v1/print/test")
            .then((response) => {
              this.fetchTest = false;
            })
            .catch((err) => {
              this.fetchTest = false;
            });
        }
      },
    },
  });
});
