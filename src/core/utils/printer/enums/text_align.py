# Standard
from enum import Enum

class TextAlign(Enum):
    L = "left"
    C = "center"
    R = "right"
