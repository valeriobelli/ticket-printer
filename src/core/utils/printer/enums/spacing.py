# Standard
from enum import Enum


class Spacing(Enum):
    N  = 0
    SM = 60
    MD = 120
    LG = 180
