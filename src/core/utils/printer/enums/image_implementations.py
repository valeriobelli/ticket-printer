# Standard
from enum import Enum


class ImageImplementations(Enum):
    BIR = "bitImageRaster"
    GRA = "graphics"
    BIC = "bitImageColumn"
