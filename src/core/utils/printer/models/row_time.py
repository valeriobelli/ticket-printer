# Internal
from src.core.utils.printer.enums import FontType, ImageImplementations, Spacing, TextAlign
from src.core.utils.printer.models import Row


class RowTime(Row):

    def __init__(
        self,
        width: int = 1,
        height: int = 1,
        font: FontType = FontType.A,
        spacing: Spacing = Spacing.N,
        align: TextAlign = TextAlign.C,
        bold: bool = False,
        underline: bool = False,
    ):
        """RowIndex constructor

        Keyword Arguments:
            width {int} -- The width of the text (default: {1})
            height {int} -- The height of the text (default: {1})
            font {FontType} -- The font to be used (default: {FontType.A})
            spacing {Spacing} -- The spacing to be used (default: {Spacing.N})
            align {TextAlign} -- The align of the text to be used (default: {TextAlign.C})
            bold {bool} -- The text should be bold? (default: {False})
            underline {bool} -- The text should be underlined? (default: {False})
        """
        super().__init__(align, spacing)

        self.width = width
        self.height = height
        self.font = FontType(font)
        self.bold = bold
        self.underline = underline

    def print(self, row_visitor):
        """Print the row to the ticket.

        Arguments:
            row_visitor {src.core.utils.printer.visitors.RowVisitor} -- [The row visitor]
        """
        row_visitor.visit_row_time(self)
