# Standard
import os

# Internal
from src.core.utils.printer.models import Row
from src.core.utils.printer.enums import ImageImplementations, Spacing, TextAlign


class RowImage(Row):
    path = ... # type: str

    def __init__(
        self,
        path: str,
        high_density_vertical: bool = True,
        high_density_horizontal: bool = True,
        impl: ImageImplementations = ImageImplementations.BIR,
        fragment_height: int = 1024,
        spacing: Spacing = Spacing.N,
        align: TextAlign = TextAlign.L,
        center: bool = False
    ):
        """
        RowImage constructor.

        :param str path:
        """
        assert isinstance(path, str), 'The variable ``path`` must be a string.'
        assert isinstance(high_density_vertical, bool), 'The variable ``high_density_vertical`` must be a bool.'
        assert isinstance(high_density_horizontal, bool), 'The variable ``high_density_horizontal`` must be a bool.'
        assert isinstance(fragment_height, int), 'The variable ``fragment_height`` must be an int.'
        assert isinstance(center, bool), 'The variable ``center`` must be a bool.'

        super().__init__(align, spacing)

        self.path = path
        self.high_density_vertical = high_density_vertical
        self.high_density_horizontal = high_density_horizontal
        self.impl = ImageImplementations(impl)
        self.fragment_height = fragment_height
        self.center = center

    def print(self, row_visitor):
        """Print the row to the ticket.

        Arguments:
            row_visitor {src.core.utils.printer.visitors.RowVisitor} -- [The row visitor]
        """
        row_visitor.visit_row_image(self)
