# Internal
from src.core.utils.printer.models import Row
from src.core.utils.printer.enums import FontType, ImageImplementations, Spacing, TextAlign


class RowText(Row):
    content = ...  # type: str
    width = ... # type: int
    height = ... # type: int
    align = ... # type: TextAlign
    font = ... # type: FontType
    bold = ... # type: bool
    underline = ... # type: bool

    def __init__(
        self,
        content: str,
        width: int = 1,
        height: int = 1,
        font: FontType = FontType.A,
        spacing: Spacing = Spacing.N,
        align: TextAlign = TextAlign.C,
        bold: bool = False,
        underline: bool = False,
    ):
        """
        TicketRow constructor.

        :param RowType type:
        :param str content:
        :param int width:
        :param int height:
        :param TextAlign align:
        :param FontType font:
        :param Spacing spacing:
        :param TextType text_type:
        """
        assert isinstance(content, str), 'The variable ``content`` must be a string.'
        assert isinstance(width, int), 'The variable ``width`` must be an integer.'
        assert width >= 0 and width <= 8, 'The variable ``width`` has an invalid value (%d).' % width
        assert isinstance(height, int), 'The variable ``height`` must be an integer.'
        assert height >= 0 and height <= 8, 'The variable ``height`` has an invalid value (%d).' % height
        assert isinstance(bold, bool), 'The variable ``bold`` must be an instance of ``bool``.'
        assert isinstance(underline, bool), 'The variable ``underline`` must be an instance of ``bool``.'

        super().__init__(align, spacing)

        self.content = content
        self.width = width
        self.height = height
        self.font = FontType(font)
        self.bold = bold
        self.underline = underline

    def print(self, row_visitor):
        """Print the row to the ticket.

        Arguments:
            row_visitor {src.core.utils.printer.visitors.RowVisitor} -- [The row visitor]
        """
        row_visitor.visit_row_text(self)
