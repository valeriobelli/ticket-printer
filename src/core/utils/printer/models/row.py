# Internal
from src.core.utils.printer.enums import Spacing, TextAlign


class Row:

    align = ... # type: TextAlign
    spacing = ... # type: Spacing

    def __init__(
        self,
        align: TextAlign,
        spacing: Spacing,
    ):
        """
        Row constructor.

        :param TextAlign align:
        :param Spacing spacing:
        """
        self.align = TextAlign(align)
        self.spacing = Spacing(spacing)
