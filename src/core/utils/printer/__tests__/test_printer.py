# Standard
import os
import logging
import sys
import time
import unittest

# Vendor
from escpos.printer import File

# Internal
from src.config import TICKET_IMAGE_PATH
from src.logger import logger
from src.core.utils.printer import print_ticket
from src.core.utils.printer.models import RowImage, RowText
from src.core.utils.printer.enums import TextAlign, ImageImplementations, Spacing

class TestPrinter(unittest.TestCase):

    def test_print_image(self):
        # Arrange
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output/ticket')
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        printer = File(path, False)
        config = [
            RowText(
                '41a SAGRA',
                width=3,
                height=3,
                bold=True,
                align=TextAlign.C
            ),
            RowImage(
                TICKET_IMAGE_PATH,
                align=TextAlign.C,
                impl=ImageImplementations.BIR
            ),
        ]

        # Act & assert
        try:
            print_ticket(config, printer, logger)
        except Exception as e:
            logging.error(e)

        assert os.path.exists(path), "The path does not exist"
        assert os.path.isfile(path), "The file is not a path"

if __name__ == "__main__":
    unittest.main()
