# Standard
import os
from datetime import datetime, time


class RowVisitor:
    _printer = ... # type: escpos.Escpos
    _tickets_repository = ... # type: src.database.models.Tickets

    def __init__(self, printer, tickets_repository):
        """RowVisitor constructor.

        Arguments:
            printer {escpos.Escpos} -- [The printer to use to print the ticket]
            tickets_repository {src.database.models.Tickets} -- [The repository to use to extract the data about tickets]
        """
        self._printer = printer
        self._tickets_repository = tickets_repository

    def visit_row_image(self, row_image) -> None:
        """Visit an instace of RowImage

        Arguments:
            row_image {src.core.utils.printer.models.RowImage} -- [The row that contains an image]
        """
        self._printer.set(
            align="center",
        )
        self._printer.image(
            os.path.join(os.getcwd(), "src", "static", "images", row_image.path),
            center=row_image.center,
            impl=row_image.impl.value,
        )

    def visit_row_text(self, row_text) -> None:
        """Visit an instance of RowText

        Arguments:
            row_text {src.core.utils.printer.models.RowText} -- [The row that contains only text]
        """
        self._printer.set(
            custom_size=True,
            width=row_text.width,
            height=row_text.height,
            align=row_text.align.value,
            font=row_text.font.value,
        )
        self._printer.text("%s\n" % row_text.content)

    def visit_row_index(self, row_index) -> None:
        ticket = self._tickets_repository.one(day=datetime.today().date())
        self._printer.set(
            custom_size=True,
            width=row_index.width,
            height=row_index.height,
            align=row_index.align.value,
            font=row_index.font.value,
        )
        self._printer.text("%s\n" % str(ticket.current_ticket))

    def visit_row_date(self, row_date) -> None:
        self._printer.set(
            custom_size=True,
            width=row_date.width,
            height=row_date.height,
            align=row_date.align.value,
            font=row_date.font.value,
        )
        self._printer.text("Data: %s" % time().strftime("%d/%m/%Y"))

    def visit_row_time(self, row_hour) -> None:
        self._printer.set(
            custom_size=True,
            width=row_hour.width,
            height=row_hour.height,
            align=row_hour.align.value,
            font=row_hour.font.value,
        )
        self._printer.text("Data: %s" % time().strftime("%H:%M"))
