# Internal
from src.database.models import Tickets


class TicketsRepository:
    _logger = ... # type: logging.Logger

    def __init__(self, logger):
        """TicketsRepository constructor.

        Arguments:
            logger {logging.Logger} -- The logger
        """
        self._logger = logger

    def delete(self, **kwargs):
        Tickets.query.delete(**kwargs)

    def filter_by(self, **kwargs) -> list:
        return Tickets.query.filter_by(**kwargs)

    def new(self, **kwargs) -> Tickets:
        return Tickets(**kwargs)

    def one(self, **kwargs) -> Tickets:
        return Tickets.query.one(**kwargs)
