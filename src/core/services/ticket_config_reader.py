# Standard
import json

# Internal
from src.core.utils.printer.models import Row, RowDate, RowImage, RowIndex, RowText, RowTime

class TicketConfigReader:
    _logger = ... # type: logging.Logger

    def __init__(self, logger):
        self._logger = logger

    def read_config(self, ticket_configuration_location: str) -> list:
        assert ticket_configuration_location is not None, "ticket_configuration_location must have a value"

        with open(ticket_configuration_location) as file:
            return map(self._map_row, json.load(file))

    def _map_row(self, row: dict) -> Row:
        row_type = row.get("type")
        config = row.get("config")

        if row_type == "text":
            return RowText(**config)
        elif row_type == "image":
            return RowImage(**config)
        elif row_type == "index":
            return RowIndex(**config)
        elif row_type == "date":
            return RowDate(**config)
        elif row_type == "time":
            return RowTime(**config)
