# Standard
import unittest

# Vendor
from escpos.printer import Dummy

# Internal
from src.logger import create_logger
from src.core.services import PrinterService
from src.core.services.__tests__.mocks import MockDatabase, MockTicketConfigReader, MockTicketsRepository, MockTicketsService
from src.core.utils.printer.visitors import RowVisitor


class TestPrinterService(unittest.TestCase):

    def setUp(self):
        self._db = MockDatabase()
        self._logger = create_logger()
        self._printer = Dummy()

    def test_printing(self):
        # Arrange
        tickets_repository = MockTicketsRepository(self._logger)

        printer_service = PrinterService(
            self._db,
            self._logger,
            RowVisitor(
                self._printer,
                tickets_repository
            ),
            MockTicketConfigReader(
                self._logger
            ),
            tickets_repository,
            MockTicketsService(
                self._db,
                self._logger,
                tickets_repository
            )
        )

        # Act
        output = printer_service.print_ticket('', self._printer)

        # Assert
        assert output is not None
        assert len(output) > 0
