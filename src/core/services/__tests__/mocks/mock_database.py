class MockSession:
    def begin(self, nested=True):
        pass

    def commit(self):
        pass


class MockDatabase:
    def __init__(self):
        self.session = MockSession()
