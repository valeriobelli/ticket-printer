# Internal
from src.core.utils.printer.models import Row, RowDate, RowImage, RowIndex, RowText, RowTime
from src.core.utils.printer.enums import FontType, ImageImplementations, Spacing, TextAlign


class MockTicketConfigReader:
    _logger = ... # type: logging.Logger

    def __init__(self, logger):
        self._logger = logger

    def read_config(self, ticket_configuration_location: str) -> list:
        return [
            RowText(
                "41a SAGRA",
                width=3,
                height=3,
                align=TextAlign.C,
                bold=True
            ),
            RowIndex(
                width=8,
                height=8,
                align=TextAlign.C,
            ),
            RowDate(
                width=2,
                height=2,
                bold=True,
                align=TextAlign.C,
            ),
            RowTime(
                width=2,
                height=2,
                align=TextAlign.C,
            ),
            RowImage(
                "img-scontrino-505x313.jpg",
                align=TextAlign.C,
                impl=ImageImplementations.BIR,
            )
        ]
