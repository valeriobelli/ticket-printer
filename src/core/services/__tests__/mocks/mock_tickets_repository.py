# Standard
from datetime import datetime

# Internal
from . import MockTickets


class MockTicketsRepository:
    _logger = ... # type: logging.Logger
    _tickets = ... # type: list

    def __init__(self, logger):
        """TicketsRepository constructor.

        Arguments:
            logger {logging.Logger} -- The logger
        """
        self._logger = logger
        self._tickets = list()

    def new(self, **kwargs) -> MockTickets:
      return MockTickets(**kwargs)

    def one(self, **kwargs) -> MockTickets:
        return MockTickets(
          id = 1,
          day = datetime.today(),
          current_ticket = 1
        )
