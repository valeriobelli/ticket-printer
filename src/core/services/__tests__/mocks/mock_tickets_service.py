# Standard
from datetime import datetime


class MockTicketsService:
    _db = ... # type: logging.Logger
    _logger = ... # type: logging.Logger
    _tickets_repository = ... # type: src.core.repositories.TicketsRepository

    def __init__(self, db, logger, tickets_repository):
        """TicketsService constructor

        Arguments:
            logger {logging.Logger} -- Instance of logging.Logger
        """
        self._db = db
        self._logger = logger
        self._tickets_repository = tickets_repository

    def create_or_update_daily_tickets(self) -> None:
        pass

    def undo_atomically_modifies(self) -> None:
        pass
