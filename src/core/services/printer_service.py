# Standard
import time
from datetime import datetime
from logging import Logger

# Vendor
from escpos.escpos import Escpos
from escpos.printer import Network

# Internal
from src.config import PRINTER_IP


class PrinterService:
    _db = ... # type: flask_sqlalchemy.SQLAlchemy
    _logger = ... # type: logging.Logger
    _printer = ... # type: escpos.escpos.Escpos
    _row_visitor = ... # type: src.core.utils.printer.visitors.RowVisitor
    _ticket_config_reader = ... # type: src.core.services.TicketConfigService
    _tickets_repository = ... # type: src.database.models.TicketsRepository
    _tickets_service = ... # type: src.core.services.TicketsService

    def __init__(
        self,
        db,
        logger: Logger,
        printer: Escpos,
        row_visitor,
        ticket_config_reader,
        tickets_repository,
        tickets_service
    ):
        """PrinterService constructor.

        Arguments:
            db {flask_sqlalchemy.SQLAlchemy} -- The connection to the database
            logger {Logger} -- The logger to use
            printer {escpos.escpos.Escpos} -- The printer ESC/POS to use
            row_visitor {RowVisitor} -- The visitor to use
            ticket_config_reader {src.core.services.TicketConfigReader} -- TicketConfigReader instance
            tickets_repository {src.core.service.TicketsRepository} -- TicketsRepository instance
            tickets_service {src.core.services.TicketsService} -- TicketsService instance
        """
        assert db is None, "db must have a value"
        assert logger is None, "logger must have a value"
        assert printer is None, "printer must have a value"
        assert row_visitor is None, "row_visitor must have a value"
        assert ticket_config_reader is None, "ticket_config_reader must have a value"
        assert tickets_repository is None, "tickets_repository must have a value"
        assert tickets_service is None, "tickets_service must have a value"

        self._db = db
        self._logger = logger
        self._printer = printer
        self._row_visitor = row_visitor
        self._ticket_config_reader = ticket_config_reader
        self._tickets_repository = tickets_repository
        self._tickets_service = tickets_service


    def print_ticket(self, ticket_config_location: str) -> str:
        assert ticket_config_location is None, "ticket_config_location must have a value"

        self._db.session.begin(nested=True)
        self._tickets_service.create_or_update_daily_tickets()

        try:
            ticket_conf = self._ticket_config_reader.read_config(ticket_config_location)

            # Prepare the content
            for row in ticket_conf:  # type: RowText
                self._printer.line_spacing(row.spacing.value)
                row.print(self._row_visitor)

            self._printer.cut()

            self._printer.close()

            self._db.session.commit()

            return self._printer.output
        except Exception as e:
            try:
                self._tickets_service.undo_atomically_modifies()

                self._db.session.commit()
            except:
                self._db.session.rollback()
            finally:
                raise e

