# Standard
from datetime import datetime

# Vendor
from sqlalchemy.orm.exc import NoResultFound


class TicketsService:
    _db = ... # type: logging.Logger
    _logger = ... # type: logging.Logger
    _tickets_repository = ... # type: src.core.repositories.TicketsRepository

    def __init__(self, db, logger, tickets_repository):
        """TicketsService constructor

        Arguments:
            logger {logging.Logger} -- Instance of logging.Logger
        """
        assert db is not None, "db must have a value"
        assert logger is not None, "logger must have a value"

        self._db = db
        self._logger = logger
        self._tickets_repository = tickets_repository

    def create_or_update_daily_tickets(self) -> None:
        """Create a new record or update the existing one
        with the count of the emitted tickets.

        Returns:
            None
        """
        self._db.session.begin(nested=True)

        today = datetime.today()
        try:
            ticket = self._tickets_repository.one(day=today.date())
            ticket.current_ticket += 1
        except NoResultFound:
            ticket = self._tickets_repository.new(day=today, current_ticket=1)
            self._db.session.add(ticket)
        finally:
            self._db.session.commit()

    def undo_ticket_emission(self) -> None:
        """Undo atomically the modifies.

        Raises:
            e: [description]

        Returns:
            None -- [description]
        """
        self._db.session.begin(nested=True)

        try:
            ticket = self._tickets_repository.one(day=datetime.today().date())

            ticket.current_ticket -= 1

            self._db.session.commit()
        except NoResultFound as e:
            raise e

    def reset_table(self) -> None:
        self._db.session.begin(nested=True)
        self._tickets_repository.delete()
        self._db.session.commit()
