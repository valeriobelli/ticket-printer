# Standard
import os

def get_path_from_here(path: str) -> str:
    here = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(here, path)


PRINTER_IP = "192.168.1.220"
DB_PATH = "sqlite:///../ticket-printer.db"
TICKET_CONFIG_PATH = get_path_from_here("src/static/ticket_configurations/cantalupo.json")
RESET_KEY = "98au9nndn2svWSa9d89aBJBSIBnajbdjbBDSBjBSdjkbc=adf"
