# Vendor
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

# Internal
from src.custom_app import CustomApp
from src.controllers import init_controllers
from src.database import init_database

app = CustomApp(__name__)

init_database(app)

init_controllers(app)
