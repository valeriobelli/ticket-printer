# Vendor
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# Internal
from src.config import DB_PATH

db = SQLAlchemy()

def init_database(app: Flask):
    # Initialize the database
    app.config['SQLALCHEMY_DATABASE_URI'] = DB_PATH
    db.init_app(app)
    db.create_all(app=app)
