# Internal
from datetime import datetime

# Vendor
from sqlalchemy import Column, Integer, Date

# Internal
from src.database import db


class Tickets(db.Model):
    __tablename__ = "tickets"

    id = Column(
        Integer(),
        primary_key=True,
        autoincrement=True
    )

    day = Column(
        Date(),
        nullable=False,
        default=datetime.today(),
    )

    current_ticket = Column(
        Integer(),
        nullable=False
    )
