# Vendor
from flask import Blueprint, make_response, request

# Internal
from src.config import RESET_KEY
from src.core.repositories import TicketsRepository
from src.core.services import TicketsService
from src.database import db
from src.database.models import Tickets
from src.logger import create_logger

controller = Blueprint('reset', __name__, url_prefix='/api/v1/reset')

logger = create_logger()

@controller.route("/")
def reset_action():
    """
    Handle the reset of the database.

    :return:
    """
    logger.info("reset_action() => called")

    reset_key = request.args.get('key', '')

    tickets_service = TicketsService(
        db,
        logger,
        TicketsRepository(logger)
    )

    if reset_key == RESET_KEY:
        tickets_service.reset_table()

        logger.info("reset_action() => tickets resetted")

        return make_response("OK."), 200
    else:
        logger.warn("reset_action() => key not valid")

        return make_response("Errore nella richiesta."), 401
