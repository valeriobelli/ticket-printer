# Vendor
from escpos.printer import Network
from flask import Blueprint, make_response, jsonify

# Internal
from src.config import TICKET_CONFIG_PATH, PRINTER_IP
from src.core.repositories import TicketsRepository
from src.core.services import PrinterService, TicketConfigReader, TicketsService
from src.core.utils.printer.visitors import RowVisitor
from src.database import db
from src.logger import create_logger

controller = Blueprint("printer", __name__, url_prefix='/api/v1/print')

logger = create_logger()

@controller.route('/')
def print_action():
    """Print action to command service to print a new ticket
    """

    try:
        tickets_repository = TicketsRepository(create_logger())

        logger.info("print_action() => called")

        printer = Network(PRINTER_IP)
        printer_service = PrinterService(
            db,
            create_logger(),
            RowVisitor(db, printer),
            printer,
            TicketConfigReader(create_logger()),
            tickets_repository,
            TicketsService(
                db,
                create_logger(),
                tickets_repository
            )
        )

        printer_service.print_ticket(TICKET_CONFIG_PATH)

        logger.info("print_action() => Ticket printed")

        return make_response(), 200
    except Exception as e:
        logger.error("print_action() => exception during the printing of the ticket", exc_info=e)

        return make_response(jsonify({
            'code': '@print_action.500.1'
        })), 500
