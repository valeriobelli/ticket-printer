# Standard
import os
import glob
from importlib import import_module

# Vendor
from flask import Flask


def init_controllers(app: Flask):
    path = os.path.dirname(__file__)
    files = filter(lambda f: os.path.basename(f) != "__init__.py", glob.glob(f"{path}/*.py"))
    for file in files:
        module_name, _ = os.path.splitext(os.path.basename(file))
        m = import_module(f".{module_name}", package="src.controllers")
        app.register_blueprint(m.controller)
