# Vendor
from flask import Blueprint, request, render_template

controller = Blueprint('index', __name__)

@controller.route('/')
def index_action():
    return render_template("index.html")
