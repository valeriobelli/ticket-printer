# Standard
import logging
import sys


def create_logger() -> logging.Logger:
    """Create a new logging.Logger

    Returns:
        logging.Logger -- [The created logger]
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    logger_handler = logging.StreamHandler(sys.stdout)
    logger_handler.setLevel(logging.DEBUG)

    logger.addHandler(logger_handler)

    return logger
